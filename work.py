import csv
import json

def do_something_with_csv():
    with open('data.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            print(' ->'.join(row))

def do_something_with_json():
	with open("dico.json") as f:
		dico = json.load(f)
	for key, value in dico.items():
		print(key, " ->", value)


def why(w):
	print((w+"?")*10)

if __name__ == '__main__':
    do_something_with_csv()
    do_something_with_json()
    why("Lolo")
